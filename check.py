__author__ = 'vladimir'

import difflib
import os
"""
    You can find git repository here: https://bitbucket.org/axazeano/easy-python-diff-file-tool
    Python with batteries :D
"""
with open('standart.txt') as std, open('input.txt') as input_file, open('output.html', 'w') as output_file:
    diff = difflib.HtmlDiff().make_file(std, input_file, 'standart file', 'checked file')
    output_file.writelines(diff)

os.system('output.html')