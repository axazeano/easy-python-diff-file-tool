# README #

Easy way to compare two files

# Requirements: #
* Python 2.7 / 3.*
* Access to write files in folder with script

# How to use it? :D #
1. Put script in folder with two files: first (has name 'standart'), second (has name 'input')
2. Script compares second file with first and create output.html file with diffs
3. Greate job! Take a coffee!